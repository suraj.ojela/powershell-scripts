# Suspends the process for the active window
#
# Requires PsSuspend from PsTools

Add-Type @"
  using System;
  using System.Runtime.InteropServices;
  public class Tricks {
    [DllImport("user32.dll")]
    public static extern IntPtr GetForegroundWindow();
}
"@

$a = [tricks]::GetForegroundWindow()

$processId = (get-process | ? { $_.mainwindowhandle -eq $a } | Select -ExpandProperty "id")

PsSuspend $processId | Out-Null

echo $processId
