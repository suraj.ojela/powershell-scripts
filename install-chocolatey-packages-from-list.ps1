Param(
		[Parameter(Mandatory=$True,Position=1)]
		[string]$pkgListFile
     )

$pkgList = (Get-Content $pkgListFile) -split "\n"

# Install packages one at a time due to character limit
foreach ($package in $pkgList) {
	choco install $package --yes --limit-output
}
