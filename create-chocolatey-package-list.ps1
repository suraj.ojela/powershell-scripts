﻿$backupDir = "D:\Backups\Windows 10\Chocolatey Package List"

if (-Not (Test-Path -Path $backupDir)) {
	New-Item -ItemType Directory -Path $backupDir | Out-Null

    if (-Not (Test-Path -Path $backupDir)) { Exit }
}

$previousList = Get-ChildItem $backupDir -Name | Select-Object -Last 1 | Get-Content
$currentList = choco list --local-only --limit-output --id-only
$previousBackupsExist = Test-Path -Path $backupDir\*

if ($previousBackupsExist) {
    $backupsAreUpToDate = -Not (Compare-Object $previousList $currentList)

    if ($backupsAreUpToDate) { Exit }
}

Set-Content -Path "$backupDir\choco_pkg_list_$(Get-Date -Format filedatetime).txt" -Value $currentList
